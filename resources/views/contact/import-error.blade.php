@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Error List') }}</div>

                <div class="card-body">
                <div class="alert alert-warning">
                    These records are not uploaded due to the following reasons.
                </div>

                <div class="container">
       
                </div>
                <table class="table mt-4" id="error-users-list">
                <thead>
                <th class="nosort">Failed Reason</th>
                <th class="nosort">Record No</th>
                <th>First Name</th>
                <th class="nosort">Last Name</th>
                <th class="nosort">Phone no.</th>
                <th class="nosort">Email</th>
                <th class="nosort">Address</th>
                <th class="nosort">Nickname</th>
                <th class="nosort">Company</th>
                </thead>
                <tbody>
                @foreach($error_entry as $key=>$user)
                @php
                if($user['reason']=='name')
                {
                    $failed_msg="First Name field is missing";
                }
                elseif($user['reason']=='mobile')
                {
                    $failed_msg="Phone no field is missing"; 
                }
                elseif($user['reason']=='digit')
                {
                    $failed_msg="Phone no digit is not valid"; 
                }
                elseif($user['reason']=='duplicate')
                {
                    $failed_msg="Phone no. already exists"; 
                }


                @endphp
        
                <tr>
                <td>{{$failed_msg}}</td>
                <td>{{$user['sequence']}}</td>
                <td>{{$user['first_name']}}</td>
                <td>{{$user['last_name']}}</td>
                <td>{{$user['contact_phone_number']}}</td>
                <td>{{$user['contact_email']}}</td>
                <td>{{$user['contact_address']}}</td>
                <td>{{$user['contact_nickname']}}</td>
                <td>{{$user['contact_company']}}</td>

                <tr>
                @endforeach
                
                </tbody>
            </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('pagescript')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function(){
        $('#error-users-list').DataTable({
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": false,
            // "order": [[ 3, "desc" ]],
            // 'columnDefs' : [{ type: 'date', 'targets': [3] }]
        });
        $('.dataTables_filter input').attr("placeholder", "Search");
    });
    </script>
@endsection