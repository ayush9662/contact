@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Contact Card') }}</div>
                <div class="card-body">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{$contact->first_name.' '.$contact->last_name}}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{$contact->contact_phone_number}}</h6>
                        <p class="card-text">{{$contact->contact_address}}</p>
                        <a  class="card-link">{{$contact->contact_email}}</a>
                        <a  class="card-link">{{$contact->contact_company}}</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
