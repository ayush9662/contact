@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Contact List') }}</div>

                <div class="card-body">
                @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                        @elseif (session('warning'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('warning') }}
                        </div>
                    @endif
                <div class="container">
                <a href="{{route('contact.create')}}">Add Contact</a><br><br>
                    <form action="{{route('contact.index')}}/search" method="POST" role="search" >
                    @csrf
                        <div class="input-group">
                            <input type="text" class="form-control" name="q"
                                placeholder="Search contact"> <span class="input-group-btn">
                                <button type="submit" class="btn btn-default">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
                        </div>
                    </form>
            </div>
                <table class="table mt-4" id="noticeTable">
                <thead>
                    <th> # </th>
                    <th> Person Name </th>
                    <th> Phone No </th>
                    <th> Email </th>
                    <th> Address </th>
                    <th> Status </th>
                    <th> Action </th>
                </thead>
                <tbody>
                @php $n=0; @endphp
                @if(count($data)>0)
                @foreach($data as $contact)
                <tr>
                <td>{{++$n}}</td>
                <td>{{$contact->first_name.' '.$contact->last_name}}</td>
                <td>{{$contact->contact_phone_number}}</td>
                <td>{{$contact->contact_email}}</td>
                <td>{{$contact->contact_address}}</td>
                <td>{{$contact->status==1?'Active':'Inactive'}}</td>
                <td><a href="{{route('contact.edit',base64_encode($contact->contactId))}}"><i class="fa fa-edit"></i></a>
                <a href="{{route('contact.show',base64_encode($contact->contactId))}}" target="_blank"><i class="fa fa-share"></i></a></td>
                <tr>
                @endforeach
                @else
                <td>{{Session::get('noresult')}}</td>

                @endif
                </tbody>
            </table>
            @if(count($data)>0)
            {{ $data->withQueryString()->links() }}
            @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
