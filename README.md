Steps to setup installation:

1. Take a pull from git or extract the zip file.
2. Run composer command for vendor folder.
3. Go to config/app.php, and set url & assest_url keys
4. Run migrations.
5. Run seeder.
6. npm install && npm run dev
7. Run the project.
8. Database file is also attached.