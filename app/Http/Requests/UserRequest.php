<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return true;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'first_name' => "required|max:25|regex:/^([a-zA-Z ']*)$/",
            'last_name' => "required|max:25|regex:/^([a-zA-Z ']*)$/",
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'phone' => 'required|unique:users,phone_number|max:10|min:10',
        ];
    }

    /**
     * Get the validation message that apply to the request.
     *
     * @return array
     */
    public function message()
    {
        return [
            
            'first_name.required' => "Please enter your first name",
            'first_name.regex' => "Please enter a valid first name",
            'last_name.required' => "Please enter your last name",
            'last_name.regex' => "Please enter a valid last name",
            'email.required' => 'Please enter your email',
            'email.unique' => "An account already exists with this email",
            'phone.required'=> 'Phone Number field is required',
            'phone.max'=> 'Please enter 10 digit phone number',
            'phone.min'=> 'Please enter 10 digit phone number',
            'password.required' => 'Password field is required',
           

        ];
    }
}
