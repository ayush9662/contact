<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
   
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'first_name' => "required|max:25|regex:/^([a-zA-Z ']*)$/",
            'last_name' => "required|max:25|regex:/^([a-zA-Z ']*)$/",
            'email' => 'required|email',
            'phone' => 'required|max:10|min:10',
            'address'=>'required|max:200',
            'nickname'=>'required|max:30',
            'company'=>'required|max:50'
        ];
    }
}
