<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'username' => "required",
            'password' => 'required',
        ];
    }

    public function message()
    {
        return [
            
            'username.required' => "Please enter your username",
            'password.required' => 'Please enter your password',
           

        ];
    }

    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors(); // Here is your array of error
        $json=array(
            "message"=>"failed",
            "errors"=>$errors
        );
         throw new HttpResponseException(response()->json($json, 422));
       
    }
}
