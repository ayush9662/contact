<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class AddContactAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'first_name' => "required|max:25|regex:/^([a-zA-Z ']*)$/",
            'last_name' => "required|max:25|regex:/^([a-zA-Z ']*)$/",
            'email' => 'required|email',
            'phone' => 'required|max:10|min:10',
            'address'=>'required|max:200',
            'nickname'=>'required|max:30',
            'company'=>'required|max:50',
            'userid'=>'required'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors(); // Here is your array of error
        $json=array(
            "message"=>"failed",
            "errors"=>$errors
        );
         throw new HttpResponseException(response()->json($json, 422));
       
    }
}
