<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class APIrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'first_name' => "required|max:25|regex:/^([a-zA-Z ']*)$/",
            'last_name' => "required|max:25|regex:/^([a-zA-Z ']*)$/",
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|max:10|unique:users,phone_number',
            'password'=>'required|min:6',
            
        ];
    }
    
    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors(); // Here is your array of error
        $json=array(
            "message"=>"failed",
            "errors"=>$errors,
            "error_type"=>"validation"
        );
         throw new HttpResponseException(response()->json($json));
       
    }
}
