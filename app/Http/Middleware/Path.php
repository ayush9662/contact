<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class Path
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $path=$request->path();
        if(Session::get('userid') && $path=='login')
        {
            return redirect()->back();

        }
        elseif(Session::get('userid') && $path=='register')
        {
            return redirect()->back();

        }
        return $next($request);
    }
}
