<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::get('userid'))
        {
            return redirect()->route('login')->with('warning','You are supposed to login first');
        }
        return $next($request);
    }
}
