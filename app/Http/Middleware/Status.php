<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Mail;
use App\User;
use App\VerifyUser;

class Status
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('status')==0)
        {
            $find_user=User::where('id',Session::get('userid'))->first();
            $verifyUser = VerifyUser::create([
                'user_id' => Session::get('userid'),
                'token' => sha1(time())
              ]);
              $data=array("name"=>$find_user->first_name,"email"=>$find_user->email,"token"=>$verifyUser->token);
              $email=$find_user->email;
                Mail::send('mail',$data,function($message) use ($email)
                {
                $message->to($email)->subject("Account Verification");
                $message->from("info@dealswid.com","Cloud Contact");
                });

            Session::forget(['status','username','userid']);
            Session::flush();
            return redirect()->route('login')->with('warning','Your account is not activated yet. Please verify it.');
        }
        return $next($request);
    }
}
