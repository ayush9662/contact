<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Contact;
use App\Share;
use DB;
use Session;
use DataTables;
use Illuminate\Support\Facades\URL;

class ContactController extends Controller
{

    
    /**
     * Show the form for displaying resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
       $data=Contact::where('userId',Session::get('userid'))->paginate(10);
        return view('contact.contact-list',compact('data'));
      
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('contact.add-contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        //
        DB::beginTransaction();
        try{
            $find_contact=Contact::where(['userId'=>Session::get('userid'),'contact_phone_number'=>$request['phone']])->first();
            if($find_contact)
            {
                return redirect()->route('contact.index')->with('warning','Contact already exist with this number') ;   
            }
            $contact= Contact::create([
                'userId'=>Session::get('userid'),
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'contact_email' => $request['email'],
                'contact_phone_number' => $request['phone'],
                'contact_address' => $request['address'],
                'contact_nickname' => $request['nickname'],
                'contact_company' => $request['company'],
            ]);
             DB::commit(); 
             return redirect()->route('contact.index')->with('success','Contact has been addded successfully') ;       
            }
        catch(\Exception $e) {   
        DB::rollback();
        throw $e;
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $id=base64_decode($id);
        $find_contact=Contact::where('contactId',$id)->first();
        if($find_contact){
//custom code
            $share=Share::create([
                'cardId'=>$id,
                'token'=>sha1(time()),
                'expireTime'=>time()+3600
            ]);
         
            return route('card',[base64_encode($id),base64_encode($share->id)]).'?token='.$share->token;
//using laravel inbuilt feature
        // return URL::temporarySignedRoute(
        //     'card', now()->addMinutes(60), ['id' => base64_encode($id)]
        // );
        }
        return redirect()->route('contact.index')->with('warning','No record is found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $id=base64_decode($id);
        $find_contact=Contact::where('contactId',$id)->first();
        if($find_contact){
            return view('contact.edit-contact',compact('find_contact'));
        }
        return redirect()->route('contact.index')->with('warning','No record is found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactRequest $request, $id)
    {
        //
        $updateData=$request->all();
        $id=base64_decode($id);
        DB::beginTransaction();
        try{
            if(isset($updateData['isUpdate']))
            {
                if($updateData['oldphone']!=$updateData['phone'])
                {
                    $find_contact=Contact::where(['userId'=>Session::get('userid'),'contact_phone_number'=>$updateData['phone']])->first();
                    return redirect()->back()->with('warning','Contact already exist with this number') ; 
                }
                $data=[
                'first_name' => $updateData['first_name'],
                'last_name' => $updateData['last_name'],
                'contact_email' => $updateData['email'],
                'contact_phone_number' => $updateData['phone'],
                'contact_address' => $updateData['address'],
                'contact_nickname' => $updateData['nickname'],
                'contact_company' => $updateData['company'],
                'status'=>$updateData['status']

                ];
                Contact::where('contactId',$id)->update($data);

                DB::commit(); 
                return redirect()->route('contact.index')->with('success','Contact has been updated successfully') ;       
            }
            return redirect()->route('contact.index')->with('success','Invalid request') ; 
           }  
           catch(\Exception $e) {   
            DB::rollback();
            throw $e;
            }
        
    }


    //view card
    public function viewCard(Request $request,$id,$shareid)
    {
        $id=base64_decode($id);
        $shareid=base64_decode($shareid);
        $token=$request->query('token');
        
        $find_entry=Share::where(['shareId'=>$shareid,'token'=>$token])->first();
            // if (! $request->hasValidSignature()) {
            //     abort(401);
            // }
            if(!$find_entry)
            {
                 abort(401);
               
            }
            elseif($find_entry->expireTime<=time()){
                 abort(401);
                
            }
            else{
                $contact=Contact::where('contactId',$id)->first();
                return view('contact.view-contact',compact('contact'));
               
            }
    }

    //search contact
    public function searchContact(Request $request){
        $q = $request->input('q');
        $data=array();
        if($q != ""){
        $data = Contact::where ( 'first_name', 'LIKE', '%' . $q . '%' )
                ->orWhere ( 'last_name', 'LIKE', '%' . $q . '%' )
                ->orWhere ( 'contact_email', 'LIKE', '%' . $q . '%' )
                ->orWhere ( 'contact_phone_number', 'LIKE', '%' . $q . '%' )
                ->paginate (10)->setPath ( '' );
        $pagination = $data->appends ( array (
           'q' => $request->input('q') 
         ) );
        if (count ( $data ) > 0)
         return view ( 'contact.contact-list',compact('data'))->withQuery ( $q );
        }
         return view ( 'contact.contact-list',compact('data'))->with("noresult",'No Details found. Try to search again !' );
       } 
    
   
}
