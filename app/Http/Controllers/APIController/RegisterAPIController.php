<?php

namespace App\Http\Controllers\APIController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\API\APIrequest;
use App\User;
use App\VerifyUser;
use DB;
use Mail;
use Illuminate\Support\Facades\Hash;

class RegisterAPIController extends Controller
{
    //

    public function signup(APIrequest $request){
        $validated = $request->validated();
        DB::beginTransaction();
        try{
            $user= User::create([
                'fname' => $request['first_name'],
                'lname' => $request['last_name'],
                'email' => $request['email'],
                'phone_number' => $request['phone'],
                'password' => Hash::make($request['password']),
            ]);
            //  event(new Registered($user));
            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => sha1(time())
              ]);
              $data=array("name"=>$request['first_name'],"email"=>$request['email'],"token"=>$verifyUser->token);
              $email=$request['email'];
                // Mail::send('mail',$data,function($message) use ($email)
                // {
                // $message->to($email)->subject("Account Verification");
                // $message->from("info@dealswid.com","Cloud Contact");
                // });
             DB::commit(); 
             return response()->json(array(
                 "message"=>"User has been registered successfully",
                 "error_type"=>'none'
             ),201); 
            }
        catch(\Exception $e) {   
        DB::rollback();
        return response()->json(array(
            "error"=>$e->getMessage()
        ),403); 
        }  
    }

    public function ayush(){
        return 'ayush';
    }
    
}
