<?php

namespace App\Http\Controllers\APIController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\API\LoginAPIrequest;
use App\User;
use Illuminate\Support\Facades\Hash;
class LoginAPIController extends Controller
{
    //
    public function signin(LoginAPIrequest $request){
        $validated = $request->validated();
          //get user detail
          $failed_login=0;
          $user_exist=User::where('email',$request->username)->orWhere('phone_number',$request->username)->first();
          if($user_exist)
          {
              //check for block account
              if($user_exist->failed_login_counter==3)
              {
                 return response()->json(
                     array(
                         'message'=>'Your account is blocked'
                     )
                     );
              }
              //match the hashed password.
              elseif (Hash::check($request->password, $user_exist->password)) {
                  User::where('id',$user_exist->id)->update(['failed_login_counter'=>0]);
                  $token = $user_exist->createToken('my-app-token')->plainTextToken;
                return response()->json(
                    array(
                        'user' => $user_exist,
                        'token' => $token
                    ),200
                );
                  
              }
              //If failed set invalidLoginCounter.
              $failed_login=$user_exist->failed_login_counter;
              $failed_login++;
              //update counter value.
              User::where('id',$user_exist->id)->update(['failed_login_counter'=>$failed_login]);
              return response()->json(array('message'=>'Invalid username & passsword'));
          }
          return response()->json(array('message'=>'These credentials do not match our records'));
      
     
    
    }
}
