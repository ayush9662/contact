<?php

namespace App\Http\Controllers\APIController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contact;
use App\User;
use App\Share;
use DB;
use App\Http\Requests\API\AddContactAPIrequest;

class ContactAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data=Contact::where('userId',$request->userid)->get();
        return response()->json([
            'message'=>'success',
            'data'=>$data
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->json(['message'=>'Resource not found'],404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddContactAPIrequest $request)
    {
        //
        $find_contact=Contact::where(['userId'=>$request->userid,'contact_phone_number'=>$request->phone])->first();
        if($find_contact)
        {
           return response()->json([
               'message'=>'Contact already exist with this number',
               "error_type"=>'validation'
           ])  ;
        }
        $contact= Contact::create([
            'userId'=>$request->userid,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'contact_email' => $request->email,
            'contact_phone_number' => $request->phone,
            'contact_address' => $request->address,
            'contact_nickname' => $request->nickname,
            'contact_company' => $request->company,
            'status'=>1
        ]);
        return response()->json([
            'message'=>'Contact has been added successfully',
            "error_type"=>'none'
        ],201)  ;     

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
        return response()->json(['message'=>'success','data'=>$contact],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
        return response()->json(["message"=>"success","data"=>$contact]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddContactAPIrequest $request, Contact $contact)
    {
        //
        if($request->phone==$request->oldPhone){
            $contact= Contact::where('contactId',$contact->contactId)->update([
                'userId'=>$request->userid,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'contact_email' => $request->email,
                'contact_phone_number' => $request->phone,
                'contact_address' => $request->address,
                'contact_nickname' => $request->nickname,
                'contact_company' => $request->company,
                'status'=>1
            ]);
            return response()->json([
                'message'=>'Contact has been added successfully',
                "error_type"=>'none'
            ],201)  ;     
    
        }
        else{
        $find_contact=Contact::where(['userId'=>$request->userid,'contact_phone_number'=>$request->phone])->first();
        if($find_contact)
        {
           return response()->json([
               'message'=>'Contact already exist with this number',
               "error_type"=>'validation'
           ])  ;
        }
        $contact= Contact::where('contactId',$contact->contactId)->update([
            'userId'=>$request->userid,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'contact_email' => $request->email,
            'contact_phone_number' => $request->phone,
            'contact_address' => $request->address,
            'contact_nickname' => $request->nickname,
            'contact_company' => $request->company,
            'status'=>1
        ]);
        return response()->json([
            'message'=>'Contact has been added successfully',
            "error_type"=>'none'
        ],201)  ;     

    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
