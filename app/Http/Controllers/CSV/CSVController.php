<?php

namespace App\Http\Controllers\CSV;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CSVRequest;
use App\Contact;
use Response;
use Session;
use DB;


class CSVController extends Controller
{
    //import csv view page

    public function import()
    {
        return view('contact.csv-import');
    }

    //upload csv code
    public function uploadCSV(CSVRequest $req)
    {
        $uploadData=$req->all();
       
        $path = $req->file('csvfile')->getRealPath();  
        $data = array_map('str_getcsv', file($path));
        $data = array_slice($data, 1);  
       if(count($data)>0)
       {
        DB::beginTransaction();  
          try {
              $error_entry=[];
            foreach($data as $key=>$importData){  
                //condition to check email unique
               $record=Contact::where(['contact_phone_number'=>trim($importData[2]),'userId'=>Session::get('userid')])->get();
               if($importData[0]=='')
               {
                   //blank first name
                   $blank_name_array=array('reason'=>'name','sequence'=>$key+1,
                   'first_name'=>isset($importData[0])?substr($importData[0],0,29):'',
                   'last_name'=>isset($importData[1])?substr($importData[1],0,29):'',
                   'contact_phone_number'=>isset($importData[2])?trim($importData[2]):'',          
                   'contact_email' =>isset($importData[1])?trim($importData[3]):'',
                   'contact_address' =>isset($importData[4])?substr($importData[4],0,50):'',
                   'contact_nickname'=>isset($importData[5])?substr($importData[5],0,50):'',
                   'contact_company'=>isset($importData[6])?substr($importData[6],0,50):'',
               );
                   array_push($error_entry,$blank_name_array);
               }
               elseif($importData[2]=='')
               {
                   //blank mobile
                   $blank_mobile_array=array('reason'=>'mobile','sequence'=>$key+1,
                   'first_name'=>isset($importData[0])?substr($importData[0],0,29):'',
                    'last_name'=>isset($importData[1])?substr($importData[1],0,29):'',
                    'contact_phone_number'=>isset($importData[2])?trim($importData[2]):'',          
                    'contact_email' =>isset($importData[3])?trim($importData[3]):'',
                    'contact_address' =>isset($importData[4])?substr($importData[4],0,50):'',
                    'contact_nickname'=>isset($importData[5])?substr($importData[5],0,50):'',
                    'contact_company'=>isset($importData[6])?substr($importData[6],0,50):'',
                   );
                   array_push($error_entry,$blank_mobile_array);
               }
               elseif(strlen($importData[2])<10 or strlen($importData[2])>10 )
               {
                   // mobile digit
                   $mobile_digit_array=array('reason'=>'digit','sequence'=>$key+1,
                   'first_name'=>isset($importData[0])?substr($importData[0],0,29):'',
                    'last_name'=>isset($importData[1])?substr($importData[1],0,29):'',
                    'contact_phone_number'=>isset($importData[2])?trim($importData[2]):'',          
                    'contact_email' =>isset($importData[3])?trim($importData[3]):'',
                    'contact_address' =>isset($importData[4])?substr($importData[4],0,50):'',
                    'contact_nickname'=>isset($importData[5])?substr($importData[5],0,50):'',
                    'contact_company'=>isset($importData[6])?substr($importData[6],0,50):'',
                   );
                   array_push($error_entry,$mobile_digit_array);
               }
               elseif(count($record)>0)
               {
                   //duplicate mobile
                   $duplicate_mobile_array=array('reason'=>'duplicate','sequence'=>$key+1,
                   'first_name'=>isset($importData[0])?substr($importData[0],0,29):'',
                    'last_name'=>isset($importData[1])?substr($importData[1],0,29):'',
                    'contact_phone_number'=>isset($importData[2])?trim($importData[2]):'',          
                    'contact_email' =>isset($importData[3])?trim($importData[3]):'',
                    'contact_address' =>isset($importData[4])?substr($importData[4],0,50):'',
                    'contact_nickname'=>isset($importData[5])?substr($importData[5],0,50):'',
                    'contact_company'=>isset($importData[6])?substr($importData[6],0,50):'',
                   );
                   array_push($error_entry,$duplicate_mobile_array);
               }
               else
               {
                $contact =  Contact::create([     
                    'userId'=>Session::get('userid'),
                    'first_name'=>isset($importData[0])?substr($importData[0],0,29):'',
                    'last_name'=>isset($importData[1])?substr($importData[1],0,29):'',
                    'contact_phone_number'=>isset($importData[2])?trim($importData[2]):'',          
                    'contact_email' =>isset($importData[3])?trim($importData[3]):'',
                    'contact_address' =>isset($importData[4])?substr($importData[4],0,50):'',
                    'contact_nickname'=>isset($importData[5])?substr($importData[5],0,50):'',
                    'contact_company'=>isset($importData[6])?substr($importData[6],0,50):'',
                ]); 

               }
               
            } 
            DB::commit(); 
            if(count($error_entry)>0)
            {
               return view('contact.import-error',compact('error_entry'));
      
            }
            else
            {
                return redirect()->route('import')->with('success','CSV file has been uploaded successfully.');
      
            }
         
        }catch(\Exception $e) { 
            DB::rollback(); 
             throw $e;
       }   
    }
    else
    {
        return redirect()->route('import')->with('warning','Please upload CSV file with atleast one entry.');
      
    }
   }  

   //export csv data
   public function export(Request $request)
   {
    $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=contactlist.csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
    ); 
    $contact=Contact::where('userId',Session::get('userid'))->orderBy('created_at', 'DESC')->get();
 
  
    $columns = array('First Name','Last Name', 'Phone Number', 'Email', 'Address','Nickname','Company','Status');

    $callback = function() use ($contact, $columns)
    {
        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);
        //$srno=0;

        foreach($contact as $review) {
            if($review->status==1)
            {
                $contact_status='Active';
            }
            elseif($review->status==0)
            {
                $contact_status='Inactive';
            }
           // $srno++;
            
            
            

            fputcsv($file, array($review->first_name,$review->last_name, $review->contact_phone_number, $review->contact_email,$review->contact_address,$review->contact_nickname,$review->contact_company,$contact_status));
        }
        fclose($file);
    };
    return Response::stream($callback, 200, $headers);
   }
}
