<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\VerifyUser;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UserRequest;
use Illuminate\Auth\Events\Registered;
use DB;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware(['path', 'back'])->except('logout');
      //  $this->middleware('guest', ['except' => ['path', 'back', 'logout']]);
    }

    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(UserRequest $request)
    {
        DB::beginTransaction();
        try{
            $user= User::create([
                'fname' => $request['first_name'],
                'lname' => $request['last_name'],
                'email' => $request['email'],
                'phone_number' => $request['phone'],
                'password' => Hash::make($request['password']),
            ]);
            //  event(new Registered($user));
            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => sha1(time())
              ]);
              $data=array("name"=>$request['first_name'],"email"=>$request['email'],"token"=>$verifyUser->token);
              $email=$request['email'];
                Mail::send('mail',$data,function($message) use ($email)
                {
                $message->to($email)->subject("Account Verification");
                $message->from("info@dealswid.com","Cloud Contact");
                });
             DB::commit(); 
             return redirect()->route('login')->with('success','Congratulations!Please verify your email for login') ;       
            }
        catch(\Exception $e) {   
        DB::rollback();
        throw $e;
        }  
        
    }
    //verify
    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
            $verifyUser->user->verified = 1;
            $verifyUser->user->save();
            $status = "Your e-mail is verified. You can now login.";
            } else {
            $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/login')->with('success', $status);
    }
}
