<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\LoginRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware(['path', 'back'])->except('logout');
      //  $this->middleware('guest', ['except' => ['path', 'back', 'logout']]);
    }

    //Login validation
    public function index(LoginRequest $request)
    {
        $loginData=$request->all();
        $username=$loginData['username'];
        $password=$loginData['password'];
        $failed_login=0;
        try{
            if(isset($loginData['isLogin']))
            {
                //get user detail
                $user_exist=User::where('email',$username)->orWhere('phone_number',$username)->first();
                if($user_exist)
                {
                    //check for block account
                    if($user_exist->failed_login_counter==3)
                    {
                        return redirect()->route('login')->with('warning','Your account has been blocked');
                    }
                    //match the hashed password.
                    elseif (Hash::check("$password", $user_exist->password)) {
                        Session::put(['userid'=>$user_exist->id,'username'=>$user_exist->fname,'status'=>$user_exist->verified]);
                        User::where('id',$user_exist->id)->update(['failed_login_counter'=>0]);
                        return redirect()->route('dashboard')->with('success','Welcome back');
                        
                    }
                    //If failed set invalidLoginCounter.
                    $failed_login=$user_exist->failed_login_counter;
                    $failed_login++;
                    //update counter value.
                    User::where('id',$user_exist->id)->update(['failed_login_counter'=>$failed_login]);
                    return redirect()->route('login')->with('warning','Invalid Credentials');
                }
                return redirect()->route('login')->with('warning','Sorry! We could not find any account with these details');
            }
            return redirect()->route('login')->with('warning','Invalid request');
        }
        catch(\Exception $e)
        {
            throw $e;
        }
        
    }

    //logout
    public function logout()
    {
        Session::forget(['status','username','userid']);
        Session::flush();
        return redirect()->route('login')->with('success','You have been logout successfully');
    }
}
