<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'contact_email','contact_phone_number', 'contact_address','contact_nickname','contact_company','userId'
    ];

    protected $gaurded = [
        'contactId'
    ];

    protected $primaryKey ='contactId';
}
