<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('contactId')->autoIncrement();
            $table->integer('userId');
            $table->index('userId');
            $table->foreign('userId')->references('id')->on('users');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone_number')->nullable();
            $table->string('contact_address')->nullable();
            $table->string('contact_nickname')->nullable();
            $table->string('contact_company')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
