<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $mobile=9451865789;
        for($i=1;$i<=5;$i++){
            User::create([
                "fname"=>str_shuffle('abcdefghijkl'),
                "lname"=>str_shuffle('abcdefghijkl'),
                "phone_number"=>$mobile++,
                "email"=>Str::random(5).'@gmail.com',
                "password"=>Hash::make(123456),
                "verified"=>1
           ]);
        }
       
      
       
    }
}
