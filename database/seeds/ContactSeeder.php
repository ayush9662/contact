<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\User;
use App\Contact;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users=User::all();
        $mobile=9001989543;
        foreach($users as $user){
            for($i=1;$i<=10;$i++)
            {
                Contact::create([
                    'userId'=>$user->id,
                    'first_name' =>str_shuffle('abcdefghijkl') ,
                    'last_name' => str_shuffle('abcdefghijkl'),
                    'contact_email' => Str::random(5).'@gmail.com',
                    'contact_phone_number' => $mobile++,
                    'contact_address' => 'Lucknow',
                    'contact_nickname' => 'Nikki',
                    'contact_company' =>'Neosoft',
                ]);
            }
            
        }
    }
}
