<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use App\Contact;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Login & Register Routes.
Auth::routes();

Route::post('/user-register', 'Auth\RegisterController@register')->name('user-register');
Route::post('/login-verify', 'Auth\LoginController@index')->name('user-login');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser')->middleware('back');
Route::get('/',function(){
    return view('welcome');
});
Route::get('/contact-card/{id}/{shareid}','Contact\ContactController@viewCard')->name('card');



//protected routes

Route::group(['middleware'=>['web','access','status','back']],function(){
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::resource('/contact','Contact\ContactController');
Route::get('/import-csv','CSV\CSVController@import')->name('import');
Route::post('/upload-csv','CSV\CSVController@uploadCSV')->name('upload');
Route::any ( 'contact/search','Contact\ContactController@searchContact');
Route::get('/logout','Auth\Logincontroller@logout')->name('logout');
Route::get('share','Contact\ContactController@shareLink')->name('linkshare');
});

Route::group(['middleware'=>['web','access','status']],function(){
    Route::get('/export-csv','CSV\CSVController@export')->name('export');
});